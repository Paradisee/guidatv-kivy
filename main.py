from kivy.app import App
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.image import AsyncImage
from kivy.uix.scrollview import ScrollView
from kivy.uix.screenmanager import ScreenManager, Screen

import utils


Builder.load_string("""
<OnAirContainer@BoxLayout>
    orientation: 'vertical'
    spacing: 1
    padding: 20
    size_hint: 1, None
    size: 0, 500

    canvas.before:
        Color:
            rgba: 134/255, 135/255, 138/255, 1

        Rectangle:
            pos: self.pos
            size: self.size

    Label:
        id: channel
        size_hint_y: None
        text_size: self.width, None
        height: self.texture_size[1]

    Label:
        id: title
        size_hint_y: None
        text_size: self.width, None
        height: self.texture_size[1]
        bold: True

    Label:
        id: hour
        size_hint_y: None
        text_size: self.width, None
        height: self.texture_size[1]

    AsyncImage:
        id: image

    Label:
        id: description
        size_hint_y: None
        text_size: self.width, None
        height: self.texture_size[1]


<LiveScreen@Screen>
    ScrollView:
        GridLayout:
            id: live_container
            orientation: 'vertical'
            spacing: 20
            padding: 20
            cols: 1
            size_hint_y: None
            height: self.minimum_height


<HomeScreen@Screen>
    BoxLayout:
        orientation: 'vertical'

        Button:
            text: 'Stasera in TV'

        GridLayout:
            id: channels_container
            orientation: 'horizontal'
            spacing: 20
            padding: 20
            cols: 3

        GridLayout:
            id: live_container
            orientation: 'vertical'
            spacing: 20
            padding: 20
            cols: 1
            size_hint_y: None
            height: self.minimum_height
""")


class OnAirContainer(BoxLayout):

    def __init__(self, item, **kwargs):
        super().__init__(**kwargs)

        self.item = item

        self.create_content()

    def create_content(self):
        self.ids.channel.text = self.item['channel']
        self.ids.title.text = self.item['name'].upper()
        self.ids.title.text = self.item['name'].upper()
        self.ids.hour.text = self.item['hour']
        self.ids.description.text = utils.wrap_text(self.item['description'])

        image = self.item['image']
        self.ids.image.source = f'https://www.raiplay.it{image}'


class LiveScreen(Screen):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def update_events(self, url):
        response = utils.get_api(url)

        for item in response['events']:
            self.ids.live_container.add_widget(OnAirContainer(item))


class HomeScreen(Screen):

    def __init__(self, app, **kwargs):
        super().__init__(**kwargs)

        self.app = app
        self.get_channels()

    def get_channels(self):
        self.get_rai_channels()
        self.get_mediaset_channels()

    def get_rai_channels(self):
        response = utils.get_api('https://www.raiplay.it/palinsesto/onAir.json')

        for item in response['on_air']:
            channel = item['channel']

            button = Button(text=channel, on_press=self.get_palinsesto_rai)
            button.channel = channel

            self.ids.channels_container.add_widget(button)

    def get_mediaset_channels(self):
        pass

    def get_palinsesto_rai(self, instance):
        channel = instance.channel.replace(' ', '-')
        date = utils.get_date()
        url = f'https://www.raiplay.it/palinsesto/app/{channel}/{date}.json'

        self.app.manager.current = 'live'
        self.app.live_screen.update_events(url)

    def get_palinsesto_mediaset(self, instance):
        pass


class GuidaTvApp(App):

    def build(self):
        self.home_screen = HomeScreen(app=self, name='home')
        self.live_screen = LiveScreen(name='live')

        self.manager = ScreenManager()
        self.manager.add_widget(self.home_screen)
        self.manager.add_widget(self.live_screen)

        return self.manager


if __name__ == '__main__':
    app = GuidaTvApp()
    app.run()
