import datetime
import requests


def get_api(url):
	return requests.get(url).json()


def wrap_text(text, length=200):
	return f'{text[:length]}...' if len(text) > length else text

def get_date():
	return datetime.datetime.today().strftime('%d-%m-%Y')
